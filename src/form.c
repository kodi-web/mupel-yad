/*
 * This file is part of YAD.
 *
 * YAD is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * YAD is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with YAD. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2008-2014, Victor Ananjevsky <ananasik@gmail.com>
 */

#include <stdlib.h>
#include <unistd.h>
#include <glib/gprintf.h>
#include <vte-0.0/vte/vte.h>

#include "yad.h"

#include "calendar.xpm"

static GSList *fields = NULL;
static FILE *cross_com_file = NULL;
static guint n_fields;

static GtkWidget *progress_bar = NULL;
static GtkWidget *progress_log = NULL;
static GtkWidget *terminal = NULL;
static GtkTextBuffer *log_buffer = NULL;

static void button_clicked_cb (GtkButton * b, gchar * action);
static void form_clicked_cb (GtkEntry * entry, gpointer data);
static void form_changed_cb (GtkEntry * entry, gpointer data);
static void form_activate_cb_id (GtkEntry * entry, gpointer data);
static gboolean handle_stdin (GIOChannel * channel, GIOCondition condition, gpointer data);
static void form_print_field (guint fn, FILE *file);

/* expand %N in command to fields values */
static GString *
expand_action (gchar * cmd)
{
  GString *xcmd;
  guint i = 0;

  xcmd = g_string_new ("");
  while (cmd[i])
    {
      if (cmd[i] == '%')
        {
          i++;
          if (g_ascii_isdigit (cmd[i]))
            {
              YadField *fld;
              gchar *buf;
              guint num, j = i;

              /* get field num */
              while (g_ascii_isdigit (cmd[j]))
                j++;
              buf = g_strndup (cmd + i, j - i);
              num = g_ascii_strtoll (buf, NULL, 10);
              g_free (buf);
              if (num > 0 && num <= n_fields)
                num--;
              else
                continue;
              /* get field value */
              fld = g_slist_nth_data (options.form_data.fields, num);
              switch (fld->type)
                {
                case YAD_FIELD_SIMPLE:
                case YAD_FIELD_HIDDEN:
                case YAD_FIELD_READ_ONLY:
                case YAD_FIELD_COMPLETE:
                case YAD_FIELD_FILE_SAVE:
                case YAD_FIELD_DIR_CREATE:
                case YAD_FIELD_MFILE:
                case YAD_FIELD_MDIR:
                case YAD_FIELD_DATE:
                  g_string_append (xcmd, gtk_entry_get_text (GTK_ENTRY (g_slist_nth_data (fields, num))));
                  break;
                case YAD_FIELD_NUM:
                  g_string_append_printf (xcmd, "%f", gtk_spin_button_get_value
                                          (GTK_SPIN_BUTTON (g_slist_nth_data (fields, num))));
                  break;
                case YAD_FIELD_CHECK:
                  g_string_append (xcmd, gtk_toggle_button_get_active
                                   (GTK_TOGGLE_BUTTON (g_slist_nth_data (fields, num))) ? "TRUE" : "FALSE");
                  break;
                case YAD_FIELD_COMBO:
                case YAD_FIELD_COMBO_ENTRY:
                  g_string_append (xcmd,
#if GTK_CHECK_VERSION(2,24,0)
                                   gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT
                                                                       (g_slist_nth_data (fields, num)))
#else
                                   gtk_combo_box_get_active_text (GTK_COMBO_BOX (g_slist_nth_data (fields, num)))
#endif
                                   );
                  break;
                case YAD_FIELD_SCALE:
                  g_string_append_printf (xcmd, "%d", (gint) gtk_range_get_value
                                          (GTK_RANGE (g_slist_nth_data (fields, num))));
                  break;
                case YAD_FIELD_FILE:
                case YAD_FIELD_DIR:
                  g_string_append (xcmd, gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (g_slist_nth_data (fields, num))));
                  break;
                case YAD_FIELD_FONT:
                  g_string_append (xcmd, gtk_font_button_get_font_name (GTK_FONT_BUTTON (g_slist_nth_data (fields, num))));
                  break;
                case YAD_FIELD_COLOR:
                  {
                    GdkColor c;

                    gtk_color_button_get_color (GTK_COLOR_BUTTON (g_slist_nth_data (fields, num)), &c);
                    buf = gdk_color_to_string (&c);
                    g_free (buf);
                    break;
                  }
                case YAD_FIELD_TEXT:
                  {
                    gchar *txt;
                    GtkTextBuffer *tb;
                    GtkTextIter b, e;

                    tb = gtk_text_view_get_buffer (GTK_TEXT_VIEW (g_slist_nth_data (fields, num)));
                    gtk_text_buffer_get_bounds (tb, &b, &e);
                    buf = gtk_text_buffer_get_text (tb, &b, &e, FALSE);
                    txt = escape_str (buf);
                    g_string_append (xcmd, txt);
                    g_free (txt);
                    g_free (buf);
                    break;
                  }
                default:;
                }
              i = j;
            }
          else
            {
              g_string_append_c (xcmd, cmd[i]);
              i++;
            }
        }
      else
        {
          g_string_append_c (xcmd, cmd[i]);
          i++;
        }
    }
  g_string_append_c (xcmd, '\0');

  return xcmd;
}

static void
set_field_value (guint num, gchar * value)
{
  GtkWidget *w;
  gchar **s;
  YadField *fld = g_slist_nth_data (options.form_data.fields, num);

  if (fld->type == YAD_FIELD_XTALK_PROC)
    return;

  w = GTK_WIDGET (g_slist_nth_data (fields, num));
  if (g_ascii_strcasecmp (value, "@disabled@") == 0)
    {
      gtk_widget_set_sensitive (w, FALSE);
      return;
    }
  else
    gtk_widget_set_sensitive (w, TRUE);

  switch (fld->type)
    {
    case YAD_FIELD_READ_ONLY:
      gtk_widget_set_sensitive (w, FALSE);
    case YAD_FIELD_SIMPLE:
    case YAD_FIELD_HIDDEN:
    case YAD_FIELD_MFILE:
    case YAD_FIELD_MDIR:
    case YAD_FIELD_FILE_SAVE:
    case YAD_FIELD_DIR_CREATE:
    case YAD_FIELD_DATE:
      gtk_entry_set_text (GTK_ENTRY (w), value);
      g_signal_connect (G_OBJECT (w), "activate", G_CALLBACK (form_activate_cb_id), GINT_TO_POINTER(num));
      break;

    case YAD_FIELD_NUM:
      s = g_strsplit (value, options.common_data.item_separator, -1);
      if (s[0])
        {
          gdouble val = g_ascii_strtod (s[0], NULL);
          w = g_slist_nth_data (fields, num);
          if (s[1])
            {
              gdouble min, max;
              gchar **s1 = g_strsplit (s[1], "..", 2);
              min = g_ascii_strtod (s1[0], NULL);
              max = g_ascii_strtod (s1[1], NULL);
              g_strfreev (s1);
              gtk_spin_button_set_range (GTK_SPIN_BUTTON (w), min, max);
              if (s[2])
                {
                  gdouble step = g_ascii_strtod (s[2], NULL);
                  gtk_spin_button_set_increments (GTK_SPIN_BUTTON (w), step, step * 10);
                  if (s[3])
                    {
                      guint prec = (guint) g_ascii_strtoull (s[3], NULL, 0);
                      if (prec > 20)
                        prec = 20;
                      gtk_spin_button_set_digits (GTK_SPIN_BUTTON (w), prec);
                    }
                }
            }
          /* set initial value must be after setting range and step */
          gtk_spin_button_set_value (GTK_SPIN_BUTTON (w), val);
        }
      g_strfreev (s);
      g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
      break;

    case YAD_FIELD_CHECK:
      if (g_ascii_strcasecmp (value, "TRUE") == 0)
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), TRUE);
      else
        gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (w), FALSE);
      g_signal_connect (G_OBJECT (w), "clicked", G_CALLBACK (form_clicked_cb), GINT_TO_POINTER(num));
      break;

    case YAD_FIELD_COMPLETE:
      {
        GtkEntryCompletion *c;
        GtkTreeModel *m;
        GtkTreeIter it;
        gint i = 0, def = -1;

        c = gtk_entry_get_completion (GTK_ENTRY (w));
        m = gtk_entry_completion_get_model (GTK_ENTRY_COMPLETION (c));
        gtk_list_store_clear (GTK_LIST_STORE (m));

        s = g_strsplit (value, options.common_data.item_separator, -1);
        while (s[i])
          {
            gtk_list_store_append (GTK_LIST_STORE (m), &it);
            if (s[i][0] == '^')
              {
                gtk_list_store_set (GTK_LIST_STORE (m), &it, 0, g_strcompress (s[i] + 1), -1);
                def = i;
              }
            else
              gtk_list_store_set (GTK_LIST_STORE (m), &it, 0, g_strcompress (s[i]), -1);

            i++;
          }
        if (def >= 0)
          gtk_entry_set_text (GTK_ENTRY (w), s[def]);
        g_strfreev (s);
        g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
        break;
     }

    case YAD_FIELD_COMBO:
    case YAD_FIELD_COMBO_ENTRY:
      {
        GtkTreeModel *m;
        gint i = 0, def = 0;

        /* cleanup previous values */
        m = gtk_combo_box_get_model (GTK_COMBO_BOX (w));
        gtk_list_store_clear (GTK_LIST_STORE (m));

        s = g_strsplit (value, options.common_data.item_separator, -1);
        while (s[i])
          {
            gchar *buf;

            if (s[i][0] == '^')
              {
                buf = g_strcompress (s[i] + 1);
                def = i;
              }
            else
              buf = g_strcompress (s[i]);
#if GTK_CHECK_VERSION(2,24,0)
            gtk_combo_box_text_append_text (GTK_COMBO_BOX_TEXT (w), buf);
#else
            gtk_combo_box_append_text (GTK_COMBO_BOX (w), buf);
#endif
            g_free (buf);
            i++;
          }
        gtk_combo_box_set_active (GTK_COMBO_BOX (w), def);
        g_strfreev (s);
        g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
        break;
      }

    case YAD_FIELD_DIR:
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (w), value);
      g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
    case YAD_FIELD_FILE:
      gtk_file_chooser_set_filename (GTK_FILE_CHOOSER (w), value);
      g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
      break;

    case YAD_FIELD_FONT:
      gtk_font_button_set_font_name (GTK_FONT_BUTTON (w), value);
      g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
      break;

    case YAD_FIELD_SCALE:
      gtk_range_set_value (GTK_RANGE (w), atoi (value));
      g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
      break;

    case YAD_FIELD_COLOR:
      {
        GdkColor c;

        gdk_color_parse (value, &c);
        gtk_color_button_set_color (GTK_COLOR_BUTTON (w), &c);
        g_signal_connect (G_OBJECT (w), "changed", G_CALLBACK (form_changed_cb), GINT_TO_POINTER(num));
        break;
      }

    case YAD_FIELD_BUTTON:
    case YAD_FIELD_FULL_BUTTON:
      g_signal_connect (G_OBJECT (w), "clicked", G_CALLBACK (button_clicked_cb), value);
      break;

    case YAD_FIELD_TEXT:
      {
        GtkTextBuffer *tb = gtk_text_view_get_buffer (GTK_TEXT_VIEW (w));
        gchar *txt = g_strcompress (value);
        gtk_text_buffer_set_text (tb, txt, -1);
        g_free (txt);
        break;
      }

    default: ;
    }
}

static void
button_clicked_cb (GtkButton * b, gchar * action)
{
  static gchar *newline = NULL;

  if (!newline)
    newline = g_strcompress ("\n");

  if (action && action[0])
    {
      if (action[0] == '@')
        {
          gchar *data;
          gint exit = 1;
          GString *cmd = expand_action (action + 1);
          g_spawn_command_line_sync (cmd->str, &data, NULL, &exit, NULL);
          if (exit == 0)
            {
              guint i = 0;
              gchar **lines = g_strsplit (data, "\n", 0);
              while (lines[i] && lines[i][0])
                {
                  gint fn;
                  gchar **ln;

                  if (!lines[i][0])
                    continue;

                  ln = g_strsplit (lines[i], ":", 2);
                  fn = g_ascii_strtoll (ln[0], NULL, 10);
                  if (fn && ln[1])
                    set_field_value (fn - 1, ln[1]);
                  g_strfreev (ln);
                  i++;
                }
            }
          g_free (data);
          g_string_free (cmd, TRUE);
        }
      else
        {
          GString *cmd = expand_action (action);
          g_spawn_command_line_async (cmd->str, NULL);
          g_string_free (cmd, TRUE);
        }
    }
}

static void
handle_xcom_log (guint value)
{
  if ((cross_com_file = g_fopen (options.form_data.xcom_log, "a+")) != NULL)
    {
      g_fprintf (cross_com_file, "#< msg field %i:", value);
      form_print_field (value, cross_com_file);
      g_fprintf (cross_com_file, ";\n");
      fflush (cross_com_file);
      if (fsync(fileno(cross_com_file)) || fclose(cross_com_file))
        {
          g_printerr ("Error writing configuration data to file");
        }
    }
  else
    {
      g_printerr ("Not possible to open cross com file '%s'\n", options.form_data.xcom_log);
    }
}

static void
form_activate_cb (GtkEntry * entry, gpointer data)
{
  if (options.plug == -1)
    gtk_dialog_response (GTK_DIALOG (data), YAD_RESPONSE_OK);
}

static void
form_activate_cb_id (GtkEntry * entry, gpointer data)
{
  if (options.form_data.xcom_log)
    handle_xcom_log(GPOINTER_TO_INT (data));
}

static void
form_clicked_cb (GtkEntry * entry, gpointer data)
{
  if (options.form_data.xcom_log)
    handle_xcom_log(GPOINTER_TO_INT (data));
}

static void
form_changed_cb (GtkEntry * entry, gpointer data)
{
  if (options.form_data.xcom_log)
    handle_xcom_log(GPOINTER_TO_INT (data));
}

static void
select_files_cb (GtkEntry * entry, GtkEntryIconPosition pos, GdkEventButton * event, gpointer data)
{
  GtkWidget *dlg;
  static gchar *path = NULL;

  if (event->button == 1)
    {
      YadFieldType type = GPOINTER_TO_INT (data);

      if (!path)
        {
          const gchar *val = gtk_entry_get_text (entry);

          if (g_file_test (val, G_FILE_TEST_IS_DIR))
            path = g_strdup (val);
          else
            path = val ? g_path_get_dirname (val) : g_get_current_dir ();
        }

      if (type == YAD_FIELD_MFILE)
        {
          dlg = gtk_file_chooser_dialog_new (_("Select files"),
                                             GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (entry))),
                                             GTK_FILE_CHOOSER_ACTION_OPEN,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);
        }
      else
        {
          dlg = gtk_file_chooser_dialog_new (_("Select folders"),
                                             GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (entry))),
                                             GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);
        }
      gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER (dlg), TRUE);
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dlg), path);

      if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_ACCEPT)
        {
          GSList *files, *ptr;
          GString *str;

          str = g_string_new ("");
          files = ptr = gtk_file_chooser_get_uris (GTK_FILE_CHOOSER (dlg));

          while (ptr)
            {
              if (ptr->data)
                {
                  gchar *fn = g_filename_from_uri ((gchar *) ptr->data, NULL, NULL);
                  g_string_append (str, fn);
                  g_string_append (str, options.common_data.item_separator);
                  g_free (fn);
                }
              ptr = ptr->next;
            }

          str->str[str->len-1] = '\0'; // remove last item separator
          gtk_entry_set_text (entry, str->str);

          g_slist_free (files);
          g_string_free (str, TRUE);
        }

      g_free (path);
      path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (dlg));
      gtk_widget_destroy (dlg);
    }
}

static void
create_files_cb (GtkEntry * entry, GtkEntryIconPosition pos, GdkEventButton * event, gpointer data)
{
  GtkWidget *dlg;
  static gchar *path = NULL;

  if (event->button == 1)
    {
      YadFieldType type = GPOINTER_TO_INT (data);

      if (!path)
        {
          const gchar *val = gtk_entry_get_text (entry);

          if (g_file_test (val, G_FILE_TEST_IS_DIR))
            path = g_strdup (val);
          else
            path = val ? g_path_get_dirname (val) : g_get_current_dir ();
        }

      if (type == YAD_FIELD_FILE_SAVE)
        {
          dlg = gtk_file_chooser_dialog_new (_("Select or create file"),
                                             GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (entry))),
                                             GTK_FILE_CHOOSER_ACTION_SAVE,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);
        }
      else
        {
          dlg = gtk_file_chooser_dialog_new (_("Select or create folder"),
                                             GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (entry))),
                                             GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER,
                                             GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                             GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);
        }
      gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (dlg), path);

      if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_ACCEPT)
        {
          gchar *file = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));

          gtk_entry_set_text (entry, file);
          g_free (file);
        }

      g_free (path);
      path = gtk_file_chooser_get_current_folder (GTK_FILE_CHOOSER (dlg));
      gtk_widget_destroy (dlg);
    }
}

static void
date_selected_cb (GtkCalendar * c, gpointer data)
{
  gtk_dialog_response (GTK_DIALOG (data), GTK_RESPONSE_ACCEPT);
}

static void
select_date_cb (GtkEntry * entry, GtkEntryIconPosition pos, GdkEventButton * event, gpointer data)
{
  GtkWidget *dlg, *cal;

  if (event->button == 1)
    {
      GDate *d;

      dlg = gtk_dialog_new_with_buttons (_("Select date"),
                                         GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (entry))),
                                         GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
                                         GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                         GTK_STOCK_OK, GTK_RESPONSE_ACCEPT, NULL);
      cal = gtk_calendar_new ();
      gtk_widget_show (cal);
      g_signal_connect (G_OBJECT (cal), "day-selected-double-click", G_CALLBACK (date_selected_cb), dlg);
      gtk_box_pack_start (GTK_BOX (gtk_dialog_get_content_area (GTK_DIALOG (dlg))), cal, TRUE, TRUE, 5);

      d = g_date_new ();
      g_date_set_parse (d, gtk_entry_get_text (entry));
      if (g_date_valid (d))
        {
          gtk_calendar_select_day (GTK_CALENDAR (cal), g_date_get_day (d));
          gtk_calendar_select_month (GTK_CALENDAR (cal), g_date_get_month (d) - 1, g_date_get_year (d));
        }
      g_date_free (d);

      if (gtk_dialog_run (GTK_DIALOG (dlg)) == GTK_RESPONSE_ACCEPT)
        {
          guint day, month, year;
          gchar *format = options.common_data.date_format;
          gchar time_string[128];

          if (format == NULL)
            format = "%x";

          gtk_calendar_get_date (GTK_CALENDAR (cal), &day, &month, &year);
          d = g_date_new_dmy (year, month + 1, day);
          g_date_strftime (time_string, 127, format, d);
          gtk_entry_set_text (entry, time_string);
          g_date_free (d);
        }
      gtk_widget_destroy (dlg);
    }
}

GtkWidget *
form_create_widget (GtkWidget * dlg)
{
  GtkWidget *sw, *vp, *tbl;
  GtkWidget *w = NULL;
  guint left_attach;
  guint right_attach;
  guint top_attach;
  guint bottom_attach;

  if (options.form_data.fields)
    {
      GtkWidget *l, *e;
      GdkPixbuf *pb;
      guint i, col, row, rows;

      n_fields = g_slist_length (options.form_data.fields);

      row = col = 0;
      rows = n_fields / options.form_data.columns;
      if (n_fields % options.form_data.columns > 0)
        rows++;

#if !GTK_CHECK_VERSION(3,0,0)
      tbl = gtk_table_new (n_fields, 2 * options.form_data.columns, FALSE);
#else
      tbl = gtk_grid_new ();
      gtk_grid_set_row_spacing (GTK_GRID (tbl), 5);
      gtk_grid_set_column_spacing (GTK_GRID (tbl), 5);
#endif

      if (options.form_data.scroll)
        {
          /* create scrolled container */
          GtkAdjustment *hadj, *vadj;

          sw = gtk_scrolled_window_new (NULL, NULL);
          gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_NONE);
          gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

          hadj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (sw));
          vadj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (sw));

          vp = gtk_viewport_new (hadj, vadj);
          gtk_viewport_set_shadow_type (GTK_VIEWPORT (vp), GTK_SHADOW_NONE);
          gtk_container_add (GTK_CONTAINER (sw), vp);

          gtk_container_add (GTK_CONTAINER (vp), tbl);
          w = sw;
        }
      else
        w = tbl;

      /* create form */
      int mul;
      for (i = 0; i < n_fields; i++)
        {
          YadField *fld = g_slist_nth_data (options.form_data.fields, i);

          if (fld->left_attach == INT_MAX)
            {
              mul = 1;
              left_attach = col * 2;
              right_attach = 1 + col * 2;
              top_attach = row;
              bottom_attach = row + 1;
            }
          else
            {
              mul = 100;
              left_attach = fld->left_attach;
              right_attach = fld->right_attach;
              top_attach = fld->top_attach;
              bottom_attach = fld->bottom_attach;
            }

          /* add field label */
          l = NULL;
          if (fld->type != YAD_FIELD_CHECK && fld->type != YAD_FIELD_BUTTON &&
              fld->type != YAD_FIELD_FULL_BUTTON &&
              fld->type != YAD_FIELD_LABEL && fld->type != YAD_FIELD_TEXT &&
              fld->type != YAD_FIELD_LOG_PROC && fld->type != YAD_FIELD_TERMINAL_PROC &&
              fld->type != YAD_FIELD_XTALK_PROC)
            {
              gchar *buf = g_strcompress (fld->name);
              l = gtk_label_new (NULL);
              if (!options.data.no_markup)
                gtk_label_set_markup_with_mnemonic (GTK_LABEL (l), buf);
              else
                gtk_label_set_text_with_mnemonic (GTK_LABEL (l), buf);
              gtk_widget_set_name (l, "yad-form-flabel");
              gtk_misc_set_alignment (GTK_MISC (l), options.common_data.align, 0.5);

#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), l,
                                left_attach, right_attach,
                                top_attach, bottom_attach,
                                GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), l, left_attach, top_attach, 2, 1);
#endif
              g_free (buf);
            }

          /* add field entry */
          switch (fld->type)
            {
            case YAD_FIELD_SIMPLE:
            case YAD_FIELD_HIDDEN:
            case YAD_FIELD_READ_ONLY:
            case YAD_FIELD_COMPLETE:
              e = gtk_entry_new ();
              gtk_widget_set_name (e, "yad-form-entry");
              g_signal_connect (G_OBJECT (e), "activate", G_CALLBACK (form_activate_cb), dlg);
              if (fld->type == YAD_FIELD_HIDDEN)
                gtk_entry_set_visibility (GTK_ENTRY (e), FALSE);
              else if (fld->type == YAD_FIELD_READ_ONLY)
                gtk_widget_set_sensitive (e, FALSE);
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              if (fld->type == YAD_FIELD_COMPLETE)
                {
                  GtkEntryCompletion *c = gtk_entry_completion_new ();
                  GtkListStore *m = gtk_list_store_new (1, G_TYPE_STRING);

                  gtk_entry_set_completion (GTK_ENTRY (e), c);
                  gtk_entry_completion_set_model (c, GTK_TREE_MODEL (m));
                  gtk_entry_completion_set_text_column (c, 0);

                  g_object_unref (m);
                  g_object_unref (c);
                }

              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_NUM:
              e = gtk_spin_button_new_with_range (0.0, 65525.0, 1.0);
              gtk_entry_set_alignment (GTK_ENTRY (e), 1.0);
              gtk_widget_set_name (e, "yad-form-spin");
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_CHECK:
              {
                gchar *buf = g_strcompress (fld->name);
                e = gtk_check_button_new_with_label (buf);
                gtk_widget_set_name (e, "yad-form-check");

                if (fld->left_attach == INT_MAX)
                  left_attach = col * 2;
#if !GTK_CHECK_VERSION(3,0,0)
                gtk_table_attach (GTK_TABLE (tbl), e,
                                  left_attach, right_attach + 1 * mul,
                                  top_attach, bottom_attach,
                                  GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
                gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
                gtk_widget_set_hexpand (e, TRUE);
#endif
                fields = g_slist_append (fields, e);
                g_free (buf);
              }
              break;

            case YAD_FIELD_COMBO:
#if GTK_CHECK_VERSION(2,24,0)
              e = gtk_combo_box_text_new ();
#else
              e = gtk_combo_box_new_text ();
#endif
              gtk_widget_set_name (e, "yad-form-combo");
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_COMBO_ENTRY:
#if GTK_CHECK_VERSION(2,24,0)
              e = gtk_combo_box_text_new_with_entry ();
#else
              e = gtk_combo_box_entry_new_text ();
#endif
              gtk_widget_set_name (e, "yad-form-edit-combo");
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_FILE:
              e = gtk_file_chooser_button_new (_("Select file"), GTK_FILE_CHOOSER_ACTION_OPEN);
              gtk_widget_set_name (e, "yad-form-file");
              gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (e), g_get_current_dir ());
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_DIR:
              e = gtk_file_chooser_button_new (_("Select folder"), GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER);
              gtk_widget_set_name (e, "yad-form-file");
              gtk_file_chooser_set_current_folder (GTK_FILE_CHOOSER (e), g_get_current_dir ());
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_FONT:
              e = gtk_font_button_new ();
              gtk_widget_set_name (e, "yad-form-font");
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_COLOR:
              e = gtk_color_button_new ();
              gtk_widget_set_name (e, "yad-form-color");
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_MFILE:
            case YAD_FIELD_MDIR:
              e = gtk_entry_new ();
              gtk_widget_set_name (e, "yad-form-entry");
              gtk_entry_set_icon_from_stock (GTK_ENTRY (e), GTK_ENTRY_ICON_SECONDARY, "gtk-directory");
              g_signal_connect (G_OBJECT (e), "icon-press", G_CALLBACK (select_files_cb),
                                GINT_TO_POINTER (fld->type));
              g_signal_connect (G_OBJECT (e), "activate", G_CALLBACK (form_activate_cb), dlg);
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_FILE_SAVE:
            case YAD_FIELD_DIR_CREATE:
              e = gtk_entry_new ();
              gtk_widget_set_name (e, "yad-form-entry");
              gtk_entry_set_icon_from_stock (GTK_ENTRY (e), GTK_ENTRY_ICON_SECONDARY, "gtk-directory");
              g_signal_connect (G_OBJECT (e), "icon-press", G_CALLBACK (create_files_cb),
                                GINT_TO_POINTER (fld->type));
              g_signal_connect (G_OBJECT (e), "activate", G_CALLBACK (form_activate_cb), dlg);
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, fld->left_attach, fld->top_attach, 1, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_DATE:
              e = gtk_entry_new ();
              gtk_widget_set_name (e, "yad-form-entry");
              pb = gdk_pixbuf_new_from_xpm_data (calendar_xpm);
              gtk_entry_set_icon_from_pixbuf (GTK_ENTRY (e), GTK_ENTRY_ICON_SECONDARY, pb);
              g_object_unref (pb);
              g_signal_connect (G_OBJECT (e), "icon-press", G_CALLBACK (select_date_cb), e);
              g_signal_connect (G_OBJECT (e), "activate", G_CALLBACK (form_activate_cb), dlg);
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_SCALE:
#if !GTK_CHECK_VERSION(3,0,0)
              e = gtk_hscale_new_with_range (0.0, 100.0, 1.0);
#else
              e = gtk_scale_new_with_range (GTK_ORIENTATION_HORIZONTAL, 0.0, 100.0, 1.0);
#endif
              gtk_widget_set_name (e, "yad-form-scale");
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach + 1 * mul, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_BUTTON:
            case YAD_FIELD_FULL_BUTTON:
              e = gtk_button_new ();
              gtk_container_add (GTK_CONTAINER (e), get_label (fld->name, 2));
              gtk_widget_set_name (e, "yad-form-button");
              gtk_button_set_alignment (GTK_BUTTON (e), 0.5, 0.5);
              if (fld->type == YAD_FIELD_BUTTON)
                gtk_button_set_relief (GTK_BUTTON (e), GTK_RELIEF_NONE);
              if (fld->left_attach == INT_MAX)
                left_attach = col * 2;
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_LABEL:
              if (fld->name[0])
                {
                  gchar *buf = g_strcompress (fld->name);
                  e = gtk_label_new (NULL);
                  gtk_widget_set_name (e, "yad-form-label");
                  if (options.data.no_markup)
                    gtk_label_set_text (GTK_LABEL (e), buf);
                  else
                    gtk_label_set_markup (GTK_LABEL (e), buf);
                  gtk_label_set_line_wrap (GTK_LABEL (e), TRUE);
                  gtk_label_set_selectable (GTK_LABEL (e), options.data.selectable_labels);
                  gtk_misc_set_alignment (GTK_MISC (e), options.common_data.align, 0.5);
                  //g_signal_connect_after (G_OBJECT (e), "size-allocate", G_CALLBACK (text_size_allocate_cb), NULL);
                  g_free (buf);
                }
              else
                {
#if GTK_CHECK_VERSION(3,0,0)
                  e = gtk_separator_new (GTK_ORIENTATION_HORIZONTAL);
#else
                  e = gtk_hseparator_new ();
#endif
                  gtk_widget_set_name (e, "yad-form-separator");
                }
              if (fld->left_attach == INT_MAX)
                left_attach = col * 2;
#if !GTK_CHECK_VERSION(3,0,0)
              gtk_table_attach (GTK_TABLE (tbl), e,
                                left_attach, right_attach + 1 * mul,
                                top_attach, bottom_attach,
                                GTK_EXPAND | GTK_FILL, 0, 5, 5);
#else
              gtk_grid_attach (GTK_GRID (tbl), e, left_attach, top_attach, 2, 1);
              gtk_widget_set_hexpand (e, TRUE);
#endif
              fields = g_slist_append (fields, e);
              break;

            case YAD_FIELD_TEXT:
              {
                GtkWidget *l, *sw, *b;
                gchar *ltxt = g_strcompress (fld->name);

#if !GTK_CHECK_VERSION(3,0,0)
                b = gtk_vbox_new (FALSE, 2);
#else
                b = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
#endif

                l = gtk_label_new ("");
                gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
                if (options.data.no_markup)
                  gtk_label_set_text (GTK_LABEL (l), ltxt);
                else
                  gtk_label_set_markup (GTK_LABEL (l), ltxt);
                g_free (ltxt);
                gtk_box_pack_start (GTK_BOX (b), l, FALSE, FALSE, 0);

                sw = gtk_scrolled_window_new (NULL, NULL);
                gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_ETCHED_IN);
                gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
                gtk_box_pack_start (GTK_BOX (b), sw, TRUE, TRUE, 0);

                e = gtk_text_view_new ();
                gtk_widget_set_name (e, "yad-form-text");
                gtk_text_view_set_editable (GTK_TEXT_VIEW (e), TRUE);
                gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (e), GTK_WRAP_WORD_CHAR);
                gtk_container_add (GTK_CONTAINER (sw), e);

                if (fld->left_attach == INT_MAX)
                  left_attach = col * 2;
#if !GTK_CHECK_VERSION(3,0,0)
                gtk_table_attach (GTK_TABLE (tbl), b,
                                  left_attach, right_attach + 1 * mul,
                                  top_attach, bottom_attach,
                                  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 5, 5);
#else
                gtk_grid_attach (GTK_GRID (tbl), b, fld->left_attach, fld->top_attach, 2, 1);
                gtk_widget_set_hexpand (b, TRUE);
                gtk_widget_set_vexpand (b, TRUE);
#endif
                gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
                fields = g_slist_append (fields, e);

                break;
              }

            case YAD_FIELD_XTALK_PROC:
              {
                GIOChannel *channel;

                channel = g_io_channel_unix_new (0);
                g_io_channel_set_encoding (channel, NULL, NULL);
                g_io_channel_set_flags (channel, G_IO_FLAG_NONBLOCK, NULL);
                g_io_add_watch (channel, G_IO_IN | G_IO_HUP, handle_stdin, dlg);
                break;
              }

            case YAD_FIELD_LOG_PROC:
              {
                GtkWidget *l, *sw, *b, *ex;
                GIOChannel *channel;
                gchar *ltxt = g_strcompress (fld->name);

                // fix it when vertical specified
#if !GTK_CHECK_VERSION(3,0,0)
                b = gtk_vbox_new (FALSE, 2);
#else
                b = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
#endif

                progress_bar = gtk_progress_bar_new ();
                gtk_widget_set_name (progress_bar, "yad-progress-widget");
                gtk_widget_set_name (progress_bar, "yad-form-text");
                if (options.progress_data.log_on_top)
                  gtk_box_pack_end (GTK_BOX (b), progress_bar, FALSE, FALSE, 2);
                else
                  gtk_box_pack_start (GTK_BOX (b), progress_bar, FALSE, FALSE, 2);

                if (options.progress_data.percentage > 100)
                  options.progress_data.percentage = 100;
                gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), options.progress_data.percentage / 100.0);
                if (options.progress_data.progress_text)
                 gtk_progress_bar_set_text (GTK_PROGRESS_BAR (progress_bar), options.progress_data.progress_text);
#if GTK_CHECK_VERSION(3,0,0)
               gtk_progress_bar_set_inverted (GTK_PROGRESS_BAR (progress_bar), options.progress_data.rtl);
#else
               if (options.progress_data.rtl)
                 gtk_progress_bar_set_orientation (GTK_PROGRESS_BAR (progress_bar), GTK_PROGRESS_RIGHT_TO_LEFT);
#endif

                l = gtk_label_new ("");
                gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
                if (options.data.no_markup)
                  gtk_label_set_text (GTK_LABEL (l), ltxt);
                else
                  gtk_label_set_markup (GTK_LABEL (l), ltxt);
                g_free (ltxt);
                gtk_box_pack_start (GTK_BOX (b), l, FALSE, FALSE, 2);

                fields = g_slist_append (fields, progress_bar);

                sw = gtk_scrolled_window_new (NULL, NULL);
                gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_ETCHED_IN);
                gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
                gtk_box_pack_start (GTK_BOX (b), sw, TRUE, TRUE, 2);

                progress_log = gtk_text_view_new ();
                gtk_widget_set_name (progress_log, "yad-form-text");
                gtk_text_view_set_editable (GTK_TEXT_VIEW (progress_log), FALSE);
                gtk_widget_set_size_request (progress_log, -1, options.progress_data.log_height);
                gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (progress_log), GTK_WRAP_WORD_CHAR);
                gtk_container_add (GTK_CONTAINER (sw), progress_log);

                log_buffer = gtk_text_buffer_new (NULL);
                gtk_text_view_set_buffer (GTK_TEXT_VIEW (progress_log), log_buffer);
                gtk_text_view_set_left_margin (GTK_TEXT_VIEW (progress_log), 5);
                gtk_text_view_set_right_margin (GTK_TEXT_VIEW (progress_log), 5);
                gtk_text_view_set_editable (GTK_TEXT_VIEW (progress_log), FALSE);
                gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (progress_log), FALSE);

                if (fld->left_attach == INT_MAX)
                  left_attach = col * 2;
#if !GTK_CHECK_VERSION(3,0,0)
                gtk_table_attach (GTK_TABLE (tbl), b,
                                  left_attach, right_attach,
                                  top_attach, bottom_attach,
                                  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 5, 5);
#else
                gtk_grid_attach (GTK_GRID (tbl), b, fld->left_attach, fld->top_attach, 2, 1);
                gtk_widget_set_hexpand (b, TRUE);
                gtk_widget_set_vexpand (b, TRUE);
#endif
                channel = g_io_channel_unix_new (0);
                g_io_channel_set_encoding (channel, NULL, NULL);
                g_io_channel_set_flags (channel, G_IO_FLAG_NONBLOCK, NULL);
                g_io_add_watch (channel, G_IO_IN | G_IO_HUP, handle_stdin, dlg);

//                gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
                fields = g_slist_append (fields, terminal);
                fields = g_slist_append (fields, progress_log);

                break;
             }

            case YAD_FIELD_TERMINAL_PROC:
              {
                GtkWidget *l, *sw, *b, *ex;
                GIOChannel *channel;
                gchar *ltxt = g_strcompress (fld->name);

                // fix it when vertical specified
#if !GTK_CHECK_VERSION(3,0,0)
                b = gtk_vbox_new (FALSE, 2);
#else
                b = gtk_box_new (GTK_ORIENTATION_VERTICAL, 2);
#endif

                progress_bar = gtk_progress_bar_new ();
                gtk_widget_set_name (progress_bar, "yad-progress-widget");
                gtk_widget_set_name (progress_bar, "yad-form-text");
                if (options.progress_data.log_on_top)
                  gtk_box_pack_end (GTK_BOX (b), progress_bar, FALSE, FALSE, 2);
                else
                  gtk_box_pack_start (GTK_BOX (b), progress_bar, FALSE, FALSE, 2);

                if (options.progress_data.percentage > 100)
                  options.progress_data.percentage = 100;
                gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), options.progress_data.percentage / 100.0);
                if (options.progress_data.progress_text)
                 gtk_progress_bar_set_text (GTK_PROGRESS_BAR (progress_bar), options.progress_data.progress_text);
#if GTK_CHECK_VERSION(3,0,0)
               gtk_progress_bar_set_inverted (GTK_PROGRESS_BAR (progress_bar), options.progress_data.rtl);
#else
               if (options.progress_data.rtl)
                 gtk_progress_bar_set_orientation (GTK_PROGRESS_BAR (progress_bar), GTK_PROGRESS_RIGHT_TO_LEFT);
#endif

                l = gtk_label_new ("");
                gtk_misc_set_alignment (GTK_MISC (l), 0.0, 0.5);
                if (options.data.no_markup)
                  gtk_label_set_text (GTK_LABEL (l), ltxt);
                else
                  gtk_label_set_markup (GTK_LABEL (l), ltxt);
                g_free (ltxt);
                gtk_box_pack_start (GTK_BOX (b), l, FALSE, FALSE, 2);

                fields = g_slist_append (fields, progress_bar);

                sw = gtk_scrolled_window_new (NULL, NULL);
                gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_ETCHED_IN);
                gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
                gtk_box_pack_start (GTK_BOX (b), sw, TRUE, TRUE, 2);

                terminal = vte_terminal_new ();
//                vte_terminal_set_pty_object(VTE_TERMINAL(terminal), terminal_pty);
                vte_terminal_set_audible_bell(VTE_TERMINAL(terminal), FALSE);
                vte_terminal_set_visible_bell(VTE_TERMINAL(terminal), FALSE);
//                vte_terminal_set_scroll_on_output(VTE_TERMINAL(terminal), TRUE);
//                vte_terminal_set_allow_bold(VTE_TERMINAL(terminal), TRUE);
//                vte_terminal_set_cursor_shape(VTE_TERMINAL(terminal), VTE_CURSOR_SHAPE_BLOCK);
//                vte_terminal_set_cursor_blink_mode(VTE_TERMINAL(terminal), VTE_CURSOR_BLINK_OFF);
                gtk_container_add (GTK_CONTAINER (sw), terminal);

                if (fld->left_attach == INT_MAX)
                  left_attach = col * 2;
#if !GTK_CHECK_VERSION(3,0,0)
                gtk_table_attach (GTK_TABLE (tbl), b,
                                  left_attach, right_attach,
                                  top_attach, bottom_attach,
                                  GTK_EXPAND | GTK_FILL, GTK_EXPAND | GTK_FILL, 5, 5);
#else
                gtk_grid_attach (GTK_GRID (tbl), b, fld->left_attach, fld->top_attach, 2, 1);
                gtk_widget_set_hexpand (b, TRUE);
                gtk_widget_set_vexpand (b, TRUE);
#endif
                channel = g_io_channel_unix_new (0);
                g_io_channel_set_encoding (channel, NULL, NULL);
                g_io_channel_set_flags (channel, G_IO_FLAG_NONBLOCK, NULL);
                g_io_add_watch (channel, G_IO_IN | G_IO_HUP, handle_stdin, dlg);

//                gtk_label_set_mnemonic_widget (GTK_LABEL (l), e);
                fields = g_slist_append (fields, terminal);
                break;
             }
           }

          /* increase row and column */
          row++;
          if (row >= rows)
            {
              row = 0;
              col++;
            }
        }

      /* fill entries with data */
      if (options.extra_data)
        {
          i = 0;
          while (options.extra_data[i] && i < n_fields)
            {
              set_field_value (i, options.extra_data[i]);
              i++;
            }
        }
    }

  return w;
}

static void
form_print_field (guint fn, FILE *file)
{
  YadField *fld = g_slist_nth_data (options.form_data.fields, fn);

  switch (fld->type)
    {
    case YAD_FIELD_SIMPLE:
    case YAD_FIELD_HIDDEN:
    case YAD_FIELD_READ_ONLY:
    case YAD_FIELD_COMPLETE:
    case YAD_FIELD_MFILE:
    case YAD_FIELD_MDIR:
    case YAD_FIELD_FILE_SAVE:
    case YAD_FIELD_DIR_CREATE:
    case YAD_FIELD_DATE:
      if (options.common_data.quoted_output)
        {
          gchar *buf = g_shell_quote (gtk_entry_get_text (GTK_ENTRY (g_slist_nth_data (fields, fn))));
          g_fprintf (file, "%s%s", buf, options.common_data.separator);
          g_free (buf);
        }
      else
        g_fprintf (file, "%s%s", gtk_entry_get_text (GTK_ENTRY (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      break;
    case YAD_FIELD_NUM:
      if (options.common_data.quoted_output)
        g_fprintf (file, "'%f'%s", gtk_spin_button_get_value (GTK_SPIN_BUTTON (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      else
        g_fprintf (file, "%f%s", gtk_spin_button_get_value (GTK_SPIN_BUTTON (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      break;
    case YAD_FIELD_CHECK:
      if (options.common_data.quoted_output)
        g_fprintf (file, "'%s'%s", gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (g_slist_nth_data (fields, fn))) ? "TRUE" :
                  "FALSE", options.common_data.separator);
      else
        g_fprintf (file, "%s%s", gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (g_slist_nth_data (fields, fn))) ? "TRUE" :
                  "FALSE", options.common_data.separator);
      break;
    case YAD_FIELD_COMBO:
    case YAD_FIELD_COMBO_ENTRY:
      if (options.common_data.quoted_output)
        {
#if GTK_CHECK_VERSION(2,24,0)
          gchar *buf = g_shell_quote (gtk_combo_box_text_get_active_text
                                      (GTK_COMBO_BOX_TEXT (g_slist_nth_data (fields, fn))));
#else
          gchar *buf = g_shell_quote (gtk_combo_box_get_active_text
                                      (GTK_COMBO_BOX (g_slist_nth_data (fields, fn))));
#endif
          g_fprintf (file, "%s%s", buf, options.common_data.separator);
          g_free (buf);
        }
      else
        {
          g_fprintf (file, "%s%s",
#if GTK_CHECK_VERSION(2,24,0)
                    gtk_combo_box_text_get_active_text (GTK_COMBO_BOX_TEXT (g_slist_nth_data (fields, fn))),
#else
                    gtk_combo_box_get_active_text (GTK_COMBO_BOX (g_slist_nth_data (fields, fn))),
#endif
                    options.common_data.separator);
        }
      break;
    case YAD_FIELD_FILE:
    case YAD_FIELD_DIR:
      if (options.common_data.quoted_output)
        {
          gchar *buf = g_shell_quote (gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (g_slist_nth_data (fields, fn))));
          g_fprintf (file, "%s%s", buf, options.common_data.separator);
          g_free (buf);
        }
      else
        g_fprintf (file, "%s%s", gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      break;
    case YAD_FIELD_FONT:
      if (options.common_data.quoted_output)
        g_fprintf (file, "'%s'%s", gtk_font_button_get_font_name (GTK_FONT_BUTTON (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      else
        g_fprintf (file, "%s%s", gtk_font_button_get_font_name (GTK_FONT_BUTTON (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      break;
    case YAD_FIELD_COLOR:
      {
        GdkColor c;

        gtk_color_button_get_color (GTK_COLOR_BUTTON (g_slist_nth_data (fields, fn)), &c);
        if (options.common_data.quoted_output)
          g_fprintf (file, "'%s'%s", gdk_color_to_string (&c), options.common_data.separator);
        else
          g_fprintf (file, "%s%s", gdk_color_to_string (&c), options.common_data.separator);
        break;
      }
    case YAD_FIELD_SCALE:
      if (options.common_data.quoted_output)
        g_fprintf (file, "'%d'%s", (gint) gtk_range_get_value (GTK_RANGE (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      else
        g_fprintf (file, "%d%s", (gint) gtk_range_get_value (GTK_RANGE (g_slist_nth_data (fields, fn))),
                  options.common_data.separator);
      break;
    case YAD_FIELD_BUTTON:
    case YAD_FIELD_FULL_BUTTON:
    case YAD_FIELD_LABEL:
      if (options.common_data.quoted_output)
        g_fprintf (file, "''%s", options.common_data.separator);
      else
        g_fprintf (file, "%s", options.common_data.separator);
      break;
    case YAD_FIELD_TEXT:
      {
        gchar *txt;
        GtkTextBuffer *tb;
        GtkTextIter b, e;

        tb = gtk_text_view_get_buffer (GTK_TEXT_VIEW (g_slist_nth_data (fields, fn)));
        gtk_text_buffer_get_bounds (tb, &b, &e);
        txt = escape_str (gtk_text_buffer_get_text (tb, &b, &e, FALSE));
        g_fprintf (file, "%s%s", txt, options.common_data.separator);
        g_free (txt);
        break;
      }
    }
}

void
form_print_result (void)
{
  guint i;

  if (options.form_data.output_by_row)
    {
      guint j, rows;

      rows = n_fields / options.form_data.columns;
      rows += (n_fields % options.form_data.columns ? 1 : 0);
      for (i = 0; i < rows; i++)
        {
          for (j = 0; j < options.form_data.columns; j++)
            {
              guint fld = i + rows * j;
              if (fld < n_fields)
                form_print_field (fld, stdout);
            }
        }
    }
  else
    {
      for (i = 0; i < n_fields; i++)
        form_print_field (i, stdout);
    }
  g_fprintf (stdout, "\n");
}

static gboolean
pulsate_progress_bar (gpointer user_data)
{
  gtk_progress_bar_pulse (GTK_PROGRESS_BAR (progress_bar));
  return TRUE;
}

static gboolean
handle_stdin (GIOChannel * channel, GIOCondition condition, gpointer data)
{
  static guint pulsate_timeout = 0;
  float percentage = 0.0;

  if ((condition == G_IO_IN) || (condition == G_IO_IN + G_IO_HUP))
    {
      GString *string;
      GError *err = NULL;

      string = g_string_new (NULL);

      while (channel->is_readable != TRUE);

      do
        {
          gint status;

          do
            {
              status = g_io_channel_read_line_string (channel, string, NULL, &err);
              while (gtk_events_pending ())
                gtk_main_iteration ();
              usleep(10);
            }
          while (status == G_IO_STATUS_AGAIN);

          if (status != G_IO_STATUS_NORMAL)
            {
              if (err)
                {
                  g_printerr ("yad_progress_handle_stdin(): %s\n", err->message);
                  g_error_free (err);
                  err = NULL;
                }
              /* stop handling */
              g_io_channel_shutdown (channel, TRUE, NULL);
              return FALSE;
            }

          if (strncasecmp (string->str, "#> msg", 6) == 0)
            {
              if (strlen(string->str) <= 7)
                continue;

              gchar *str_msg = g_strcompress (g_strstrip (string->str + 7));

              if (strncasecmp (str_msg, "PERCENT ", 8) == 0)
                {
                  // Now try to convert the thing to a number
                  percentage = atoi (str_msg+8);
                  if (percentage >= 100)
                    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), 1.0);
                  else
                    gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), percentage / 100.0);
                }
              else if (strncasecmp (str_msg, "PULSE ON", 8) == 0)
                {
                  if (options.progress_data.pulsate)
                    {
                      if (pulsate_timeout == 0)
                        pulsate_timeout = g_timeout_add (100, pulsate_progress_bar, NULL);
                    }
                }
              else if (strncasecmp (str_msg, "PULSE OFF", 9) == 0)
                {
                  if (options.progress_data.pulsate)
                    {
                      g_source_remove (pulsate_timeout);
                      pulsate_timeout = 0;
                    }
                }
              else if (strncasecmp (str_msg, "FIELD ", 6) == 0)
                {
                  if (strlen(str_msg) <= 6)
                    continue;

                  if (options.form_data.xcom_log == NULL)
                    {
                      g_printerr ("Not possible to use cross com without file!\n");
                      continue;
                    }

                  gchar **ln = g_strsplit (str_msg, ":", 2);
                  gchar **value = g_strsplit (ln[1], ";", 0);
                  gint fn = g_ascii_strtoll (ln[0]+6, NULL, 10);

                  GtkWidget *w = GTK_WIDGET (g_slist_nth_data (fields, fn));
                  if (w)
                    {
                      if (strncasecmp (value[0], "enable ", 7) == 0)
                        gtk_widget_set_sensitive (w, atoi(value[0]+7) == 0 ? FALSE : TRUE);
                      else if (strncasecmp (value[0], "visible ", 8) == 0)
                        gtk_widget_set_visible (w, atoi(value[0]+8) == 0 ? FALSE : TRUE);
                      else if (strncasecmp (value[0], "set ", 4) == 0)
                        set_field_value (fn, value[0]+4);
                      else if (strncasecmp (value[0], "clear", 4) == 0)
                      {
                        YadField *fld = g_slist_nth_data (options.form_data.fields, fn-1);
                        if (fld)
                          {
                            switch (fld->type)
                              {
                              case YAD_FIELD_COMBO:
                              case YAD_FIELD_COMBO_ENTRY:
                                {
        #if GTK_CHECK_VERSION(3,0,0)
                                  gtk_combo_box_text_remove_all (GTK_COMBO_BOX_TEXT (w));
        #else
                                  /* Clear out the list */
                                  while (gtk_tree_model_iter_n_children(gtk_combo_box_get_model(GTK_COMBO_BOX(w)), NULL) > 0)
                                    {
                                      gtk_combo_box_text_remove(GTK_COMBO_BOX_TEXT(w), 0);
                                    }
        #endif
                                  break;
                                }

                              case YAD_FIELD_TEXT:
                                {
                                  GtkTextBuffer *tb = gtk_text_view_get_buffer (GTK_TEXT_VIEW (w));
                                  gtk_text_buffer_set_text (tb, "", -1);
                                  break;
                                }

                              case YAD_FIELD_LOG_PROC:
                                {
                                  GtkTextIter start, end;

                                  /* clear text if ^L received */
                                  gtk_text_buffer_get_start_iter (log_buffer, &start);
                                  gtk_text_buffer_get_end_iter (log_buffer, &end);
                                  gtk_text_buffer_delete (log_buffer, &start, &end);
                                  gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (progress_log), &end, 0, FALSE, 0, 0);
                                  break;
                                }

                              case YAD_FIELD_TERMINAL_PROC:
                                {
                                  vte_terminal_reset(VTE_TERMINAL(terminal), TRUE, TRUE);
                                  break;
                                }

                                default: ;
                              }
                          }
                      }
                      else if (strncasecmp (value[0], "values", 6) == 0)
                      {
                        if ((cross_com_file = g_fopen (options.form_data.xcom_log, "a+")) != NULL)
                          {
                            g_fprintf (cross_com_file, "#< msg field %i:", fn);
                            if (fn < 0)
                              {
                                guint i;
                                for (i = 0; i < n_fields; i++)
                                  form_print_field (i, cross_com_file);
                              }
                            else
                              {
                                form_print_field (fn, cross_com_file);
                              }
                            g_fprintf (cross_com_file, ";\n");
                            fflush (cross_com_file);
                            if (fsync(fileno(cross_com_file)) || fclose(cross_com_file))
                              {
                                g_printerr ("Error writing configuration data to file");
                              }
                          }
                        else
                          {
                            g_printerr ("Not possible to open cross com file '%s'\n", options.form_data.xcom_log);
                          }
                      }
                    }

                  g_strfreev (value);
                  g_strfreev (ln);
                }
              else if (strncasecmp (str_msg, "ACMD", 4) == 0)
                {
                  if (strlen(str_msg) <= 4)
                    continue;

                  gchar **ln = g_strsplit (str_msg, ":", 0);
                  gchar **value = g_strsplit (ln[1], ";", 0);
                  g_spawn_command_line_async (value[0], NULL);

                  g_strfreev (value);
                  g_strfreev (ln);
                }
              else if (strncasecmp (str_msg, "CMD", 3) == 0)
                {
                  if (strlen(str_msg) <= 3)
                    continue;

                  gchar **ln = g_strsplit (str_msg, ":", 0);
                  gchar **value = g_strsplit (ln[1], ";", 0);
                  g_spawn_command_line_sync (value[0], NULL, NULL, NULL, NULL);

                  g_strfreev (value);
                  g_strfreev (ln);
                }

//                gchar
              g_free (str_msg);
            }
          else
            {
              if (progress_log != NULL)
                {
                  gchar *match;

                  /* We have a comment, so let's try to change the label or write it to the log */
                  match = g_strcompress (g_strstrip (string->str));
                  gchar *logline;
                  GtkTextIter end;

                  logline = g_strdup_printf ("%s\n", match); /* add new line */
                  gtk_text_buffer_get_end_iter (log_buffer, &end);
                  gtk_text_buffer_insert (log_buffer, &end, logline, -1);
                  g_free (logline);

                  /* scroll to end */
                  while (gtk_events_pending ())
                    gtk_main_iteration ();
                  gtk_text_buffer_get_end_iter (log_buffer, &end);
                  gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (progress_log), &end, 0, FALSE, 0, 0);

                  g_free (match);
                }
              else if (terminal != NULL)
                {
                  vte_terminal_feed(VTE_TERMINAL(terminal), string->str, strlen(string->str));
                  vte_terminal_feed(VTE_TERMINAL(terminal), "\r", 2);
                }
            }
        }
      while (g_io_channel_get_buffer_condition (channel) == G_IO_IN);
      g_string_free (string, TRUE);
    }

  if ((condition != G_IO_IN) && (condition != G_IO_IN + G_IO_HUP))
    {
      gtk_progress_bar_set_fraction (GTK_PROGRESS_BAR (progress_bar), 1.0);

      if (options.progress_data.pulsate)
        {
          g_source_remove (pulsate_timeout);
          pulsate_timeout = 0;
        }

      g_io_channel_shutdown (channel, TRUE, NULL);
      return FALSE;
    }
  return TRUE;
}
