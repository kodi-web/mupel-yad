#! /bin/sh

set -e

libtoolize --automake -f
aclocal -I m4
autoconf
autoheader
automake -a
